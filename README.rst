
Analizador de Logs de Firewall Check Point
==========================================

- Parsea los logs desde archivos .csv y guarda en una Base de Datos MongoDB para ser analizados

- Consulta la BD para diferentes estadísticas:

  - Con sumo de Ancho de Banda de la Empresa
  - Consumo de Ancho de banda y gasto de Dinero por Usuario
  - Desglose del consumo de los Usuarios por Sitios o Servicios

- Crea gráficos en tiempo real en el navegador

Dependencias
------------
- MongoDB
- PyMongo
- flot
