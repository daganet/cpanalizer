# -*- coding: utf-8 -*-
#############################################
#
# Live Log Analyzer
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#############################################
#Core Imports
import datetime
import json
import logging
import time

#tornado imports
import tornado.ioloop
import tornado.web
import tornado.options
import tornadio2

import pymongo
from bson.objectid import ObjectId
import asyncmongo


if __name__ == '__main__':
    tornado.options.parse_command_line()
    application = tornado.web.Application(
        router.apply_routes([
            (r'/dashboard', DashBoardHandler),
            (r'/()', tornado.web.StaticFileHandler, {'path': 'static/index.html'}),
        ]),

        static_path='static',
        socket_io_port = 8001,
    )

    tornadio2.SocketServer(application)

