# -*- coding: utf-8 -*-
#############################################
#
# Live Log Analyzer
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#############################################