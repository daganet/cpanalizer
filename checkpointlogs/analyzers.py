# -*- coding: utf-8 -*-
#############################################
#
# Live Log Analyzer
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#############################################
import re
import pymongo

class TotalBandWidthAndMoney(object):
    """
    Total BandWidth and Money consumed
    in a Month by all the Company.
    """
    pass


class BandWidthAndMoneyPerUser(object):
    """
    Gets Bandwith and Money Consumption
    per User x Month
    """
    def __init__(self, mongo_collection, media):
        self.mongo = mongo_collection
        self.label = 'Media: %s' % media
        self.media = media

    def run(self, time_limit):
        self.mongo.ensure_index([('time', ASCENDING),
                                 ('media', ASCENDING),
                                 ])
        N = self.mongo.find({'time': {'$gt': time_limit[0],
                                      '$lt': time_limit[1]},
                             'media': self.media,
                             }).count()
        td = time_limit[1] - time_limit[0]
        self.data = 60.0 * safe_divide(float(N), td.seconds)

class UssageByService(object):
    pass