# -*- coding: utf-8 -*-
#############################################
#
# Live Log Analyzer
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#############################################
#Base Imports
import re
from datetime import datetime


class Parser(object):
    """
    Base abstract Parser class.
    """
    pattern = None
    date_format = None
    date_ignore_pattern = None

    @classmethod
    def parse_line(cls, line):
        """Parse one line of the log file.
        """
        regex = re.compile(cls.pattern)
        m = regex.search(line)
        if m:
            data = m.groupdict()
            data = cls.post_process(data)
            if cls.date_format:
                data['time'] = cls.convert_time(data['time'])
            else:
                data['time'] = datetime.now()
            return data
        else:
            return {}

class CheckpointLogs(Parser):
    """
    Class to parse Checkpoint firewall logs
    """
    pass

